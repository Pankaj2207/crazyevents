package com.crazy.events.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.crazy.events.enums.EventType;
import com.crazy.events.model.Event;
import com.crazy.events.model.EventDetail;
import com.fasterxml.jackson.databind.ObjectMapper;


public class TestRateLimiter {
	 private static final String REST_URI 
     = "http://localhost:8080/CrazyEvents/rest/events/addEvents";
 
   private Client client = ClientBuilder.newClient();

   @Test
   public void addEvents() {
	   		Response response =	client
			      .target(REST_URI)
			      .request(MediaType.APPLICATION_JSON)
			      .post(Entity.entity(getEventList(), MediaType.APPLICATION_JSON));
	   			
	   			assertEquals(response.getStatus(), 200);
   }
   
   private List<Event> getEventList() {
	   List<Event> list = new ArrayList<>();
	   	
	   	Event event = new Event();
	   	event.setId(1L);
	   	event.setEventType(EventType.EVENT_ONE);
	   	EventDetail eventDetail = new EventDetail();
	   	eventDetail.setDescription("Event One batch sample description :" + event.getId());
	   	eventDetail.setValue("Test -"+event.getId());
	   	eventDetail.setName("Event-Name-"+event.getId());
	   	event.setEventDetail(eventDetail);
	   	
	   	list.add(event);
	   	
	   	event = new Event();
	   	event.setId(2L);
	   	event.setEventType(EventType.EVENT_TWO);
	   	eventDetail = new EventDetail();
	   	eventDetail.setDescription("Event Two batch sample description :" + event.getId());
	   	eventDetail.setValue("Test -"+event.getId());
	   	eventDetail.setName("Event-Name-"+event.getId());
	   	event.setEventDetail(eventDetail);
	   	
	   	list.add(event);
	   	
	   	event = new Event();
	   	event.setId(3L);
	   	event.setEventType(EventType.EVENT_THREE);
	   	eventDetail = new EventDetail();
	   	eventDetail.setDescription("Event Three batch sample description :" + event.getId());
	   	eventDetail.setValue("Test -"+event.getId());
	   	eventDetail.setName("Event-Name-"+event.getId());
	   	event.setEventDetail(eventDetail);
	   	
	   	list.add(event);
	   	
	   	event = new Event();
	   	event.setId(4L);
	   	event.setEventType(EventType.EVENT_FOUR);
	   	eventDetail = new EventDetail();
	   	eventDetail.setDescription("Event Four batch sample description :" + event.getId());
	   	eventDetail.setValue("Test -"+event.getId());
	   	eventDetail.setName("Event-Name-"+event.getId());
	   	event.setEventDetail(eventDetail);
	   	
	   	list.add(event);
	   	
	   	event = new Event();
	   	event.setId(5L);
	   	event.setEventType(EventType.EVENT_ONE);
	   	eventDetail = new EventDetail();
	   	eventDetail.setDescription("Event One batch sample description :" + event.getId());
	   	eventDetail.setValue("Test -"+event.getId());
	   	eventDetail.setName("Event-Name-"+event.getId());
	   	event.setEventDetail(eventDetail);
	   	
	   	list.add(event);
	   	 return list;
   }
   
   public String jsonBuilder() {
	   List<Event> list = new ArrayList<>();
   	
   	Event event = new Event();
   	event.setId(1L);
   	event.setEventType(EventType.EVENT_ONE);
   	EventDetail eventDetail = new EventDetail();
   	eventDetail.setDescription("Event One batch sample description :" + event.getId());
   	eventDetail.setValue("Test -"+event.getId());
   	eventDetail.setName("Event-Name-"+event.getId());
   	event.setEventDetail(eventDetail);
   	
   	list.add(event);
   	
   	event = new Event();
   	event.setId(2L);
   	event.setEventType(EventType.EVENT_TWO);
   	eventDetail = new EventDetail();
   	eventDetail.setDescription("Event Two batch sample description :" + event.getId());
   	eventDetail.setValue("Test -"+event.getId());
   	eventDetail.setName("Event-Name-"+event.getId());
   	event.setEventDetail(eventDetail);
   	
   	list.add(event);
   	
   	event = new Event();
   	event.setId(3L);
   	event.setEventType(EventType.EVENT_THREE);
   	eventDetail = new EventDetail();
   	eventDetail.setDescription("Event Three batch sample description :" + event.getId());
   	eventDetail.setValue("Test -"+event.getId());
   	eventDetail.setName("Event-Name-"+event.getId());
   	event.setEventDetail(eventDetail);
   	
   	list.add(event);
   	
   	event = new Event();
   	event.setId(4L);
   	event.setEventType(EventType.EVENT_FOUR);
   	eventDetail = new EventDetail();
   	eventDetail.setDescription("Event Four batch sample description :" + event.getId());
   	eventDetail.setValue("Test -"+event.getId());
   	eventDetail.setName("Event-Name-"+event.getId());
   	event.setEventDetail(eventDetail);
   	
   	list.add(event);
   	
   	event = new Event();
   	event.setId(5L);
   	event.setEventType(EventType.EVENT_ONE);
   	eventDetail = new EventDetail();
   	eventDetail.setDescription("Event One batch sample description :" + event.getId());
   	eventDetail.setValue("Test -"+event.getId());
   	eventDetail.setName("Event-Name-"+event.getId());
   	event.setEventDetail(eventDetail);
   	
   	list.add(event);
   	 
   	ObjectMapper Obj = new ObjectMapper(); 
   	String jsonStr = null;
   	
   	try { 
           jsonStr = Obj.writeValueAsString(list); 
           System.out.println(jsonStr); 
       } catch (IOException ex) { 
           ex.printStackTrace(); 
       } 
      return jsonStr;
   }
}
