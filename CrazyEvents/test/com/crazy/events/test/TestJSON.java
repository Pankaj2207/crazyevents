package com.crazy.events.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObject;

import org.junit.Test;

import com.crazy.events.enums.EventType;
import com.crazy.events.model.Event;
import com.crazy.events.model.EventDetail;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestJSON {

	    @Test
	    public void testJson() {
	    	
	    	List<Event> list = new ArrayList<>();
	    	
	    	Event event = new Event();
	    	event.setId(1L);
	    	event.setEventType(EventType.EVENT_ONE);
	    	EventDetail eventDetail = new EventDetail();
	    	eventDetail.setDescription("Event One batch sample description :" + event.getId());
	    	eventDetail.setValue("Test -"+event.getId());
	    	eventDetail.setName("Event-Name-"+event.getId());
	    	event.setEventDetail(eventDetail);
	    	
	    	list.add(event);
	    	
	    	event = new Event();
	    	event.setId(2L);
	    	event.setEventType(EventType.EVENT_TWO);
	    	eventDetail = new EventDetail();
	    	eventDetail.setDescription("Event Two batch sample description :" + event.getId());
	    	eventDetail.setValue("Test -"+event.getId());
	    	eventDetail.setName("Event-Name-"+event.getId());
	    	event.setEventDetail(eventDetail);
	    	
	    	list.add(event);
	    	
	    	event = new Event();
	    	event.setId(3L);
	    	event.setEventType(EventType.EVENT_THREE);
	    	eventDetail = new EventDetail();
	    	eventDetail.setDescription("Event Three batch sample description :" + event.getId());
	    	eventDetail.setValue("Test -"+event.getId());
	    	eventDetail.setName("Event-Name-"+event.getId());
	    	event.setEventDetail(eventDetail);
	    	
	    	list.add(event);
	    	
	    	event = new Event();
	    	event.setId(4L);
	    	event.setEventType(EventType.EVENT_FOUR);
	    	eventDetail = new EventDetail();
	    	eventDetail.setDescription("Event Four batch sample description :" + event.getId());
	    	eventDetail.setValue("Test -"+event.getId());
	    	eventDetail.setName("Event-Name-"+event.getId());
	    	event.setEventDetail(eventDetail);
	    	
	    	list.add(event);
	    	
	    	event = new Event();
	    	event.setId(5L);
	    	event.setEventType(EventType.EVENT_ONE);
	    	eventDetail = new EventDetail();
	    	eventDetail.setDescription("Event One batch sample description :" + event.getId());
	    	eventDetail.setValue("Test -"+event.getId());
	    	eventDetail.setName("Event-Name-"+event.getId());
	    	event.setEventDetail(eventDetail);
	    	
	    	list.add(event);
	    	 
	    	ObjectMapper Obj = new ObjectMapper(); 
	    	
	    	try { 
	            String jsonStr = Obj.writeValueAsString(list); 
	            System.out.println(jsonStr); 
	        } catch (IOException ex) { 
	            ex.printStackTrace(); 
	        } 
	    	
	       
	    }
	 
	}

