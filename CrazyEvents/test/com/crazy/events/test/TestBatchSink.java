package com.crazy.events.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import org.junit.Test;

import com.crazy.events.async.task.IAsyncTask;
import com.crazy.events.async.task.impl.EventAsyncTskImpl;
import com.crazy.events.enums.EventType;
import com.crazy.events.model.Event;
import com.crazy.events.model.EventDetail;
import com.crazy.events.model.Response;
import com.crazy.events.service.ICrazyEventService;
import com.crazy.events.service.impl.CrazyEventServiceImpl;

public class TestBatchSink {
	
	@Test
	public void batchProcess() {
		
		String batchId = Double.toString(Math.random());
		List<Event> events = getEventList();
		events.stream().forEach(u -> u.setBatchId(batchId));
		
		Map<EventType, List<Event>> map = events
		        .stream()
		        .collect
		            (Collectors.groupingBy(Event::getEventType));
		IAsyncTask task = new EventAsyncTskImpl(map);
		Thread newThread = new Thread(task);
		newThread.run();
		
		try {
			newThread.sleep(3000L);
			newThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Response response = service.addEvents(getEventList());
		//assertTrue(response.getBatchid()!=null);
		
	}

	private List<Event> getEventList() {
		   List<Event> list = new ArrayList<>();
		   	
		   	Event event = new Event();
		   	event.setId(1L);
		   	event.setEventType(EventType.EVENT_ONE);
		   	EventDetail eventDetail = new EventDetail();
		   	eventDetail.setDescription("Event One batch sample description :" + event.getId());
		   	eventDetail.setValue("Test -"+event.getId());
		   	eventDetail.setName("Event-Name-"+event.getId());
		   	event.setEventDetail(eventDetail);
		   	
		   	list.add(event);
		   	
		   	event = new Event();
		   	event.setId(2L);
		   	event.setEventType(EventType.EVENT_TWO);
		   	eventDetail = new EventDetail();
		   	eventDetail.setDescription("Event Two batch sample description :" + event.getId());
		   	eventDetail.setValue("Test -"+event.getId());
		   	eventDetail.setName("Event-Name-"+event.getId());
		   	event.setEventDetail(eventDetail);
		   	
		   	list.add(event);
		   	
		   	event = new Event();
		   	event.setId(3L);
		   	event.setEventType(EventType.EVENT_THREE);
		   	eventDetail = new EventDetail();
		   	eventDetail.setDescription("Event Three batch sample description :" + event.getId());
		   	eventDetail.setValue("Test -"+event.getId());
		   	eventDetail.setName("Event-Name-"+event.getId());
		   	event.setEventDetail(eventDetail);
		   	
		   	list.add(event);
		   	
		   	event = new Event();
		   	event.setId(4L);
		   	event.setEventType(EventType.EVENT_FOUR);
		   	eventDetail = new EventDetail();
		   	eventDetail.setDescription("Event Four batch sample description :" + event.getId());
		   	eventDetail.setValue("Test -"+event.getId());
		   	eventDetail.setName("Event-Name-"+event.getId());
		   	event.setEventDetail(eventDetail);
		   	
		   	list.add(event);
		   	
		   	event = new Event();
		   	event.setId(5L);
		   	event.setEventType(EventType.EVENT_ONE);
		   	eventDetail = new EventDetail();
		   	eventDetail.setDescription("Event One batch sample description :" + event.getId());
		   	eventDetail.setValue("Test -"+event.getId());
		   	eventDetail.setName("Event-Name-"+event.getId());
		   	event.setEventDetail(eventDetail);
		   	
		   	list.add(event);
		   	 return list;
	   }
}
