package com.crazy.events.sink;

import java.util.Collection;

import com.crazy.events.model.Event;

public interface ISink {

	
	public void sink(Collection<Event> events);
}
