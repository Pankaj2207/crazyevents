package com.crazy.events.sink;

import org.apache.log4j.Logger;

import com.crazy.events.enums.SinkType;

public class SinkFactory {
	
	static Logger log = Logger.getLogger(SinkFactory.class);
	
	private static final String PACKAGE = "com.crazy.events.sink.";
	
	public static ISink getSink (SinkType sinkType) {
								
								ISink obj = null;
								try {
									Class<?> cls = Class.forName(PACKAGE+sinkType.getSinkType().get(sinkType));
									obj = (ISink) cls.getConstructor().newInstance();
									log.info("Getting the sink for the sink type "+ sinkType);
									
								} catch (Throwable e) {
									log.error(" Error getting the sink for the sink type "+ sinkType, e);
								}
								return obj;
	}


}
