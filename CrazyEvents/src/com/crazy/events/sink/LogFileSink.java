package com.crazy.events.sink;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

import com.crazy.events.data.access.object.IJDBCDataAccessObject;
import com.crazy.events.data.access.object.PostgresDaoImpl;
import com.crazy.events.model.Event;
import com.crazy.events.util.ThreadPool;

public class LogFileSink implements ISink{
	
	static Logger log = Logger.getLogger(LogFileSink.class);

	public static ThreadPoolExecutor threadPoolExecutor = ThreadPool.getInstance().getThreadPoolExecutor();
	private static final String PATH = "/Users/pankaj.parashar/Documents/CrazyEvent_Sink/";
	

	@Override
	public void sink(Collection<Event> events) {
					Event event = (Event) events.toArray()[0];
					String filePath = PATH.concat(event.getEventType().getStrValue().concat(".log"));
					log.info("Publishing the event type "+event.getEventType()+" in the logger sink "+filePath);
					writeToLog(events.toString(), filePath);
					persistStatus((List<Event>)events);
		
		
	}

	private CompletableFuture<Boolean> persistStatus(List<Event> events) {
									return CompletableFuture.supplyAsync (() -> { IJDBCDataAccessObject dao = new PostgresDaoImpl(); 		
										try {
											 dao.insertEvents(events);
											 return true;
										} catch (Exception e) {
											log.error("An error occurred while persisting the status data", e);
											return false;
										}
									} , threadPoolExecutor).handle((detail, ex) -> {
										if (ex != null) {
											log.error("An error occurred while handling the completable future async task", ex);
											return false;
										}
										return true;
										});
									
									
								}

	public CompletableFuture<Boolean> writeToLog(String str, String filePath) {
		
												return CompletableFuture.supplyAsync(() -> {  PrintWriter pw;
												try {
													pw = new PrintWriter(new FileOutputStream(filePath));
													pw.println(str);
													pw.close();
													return true;
												} catch (Exception e) {
													log.error("An error occurred while writting the events in the sink : " 
															+ filePath, e);
													e.printStackTrace();
													return false;
												}
											}, threadPoolExecutor)
							.handle((detail,ex) -> {
								System.out.println(""+false);
								 if(ex != null) {
									 log.error("An error occurred while handling the completable future async task" 
											, ex);
									 return false;
					    		        
					    		    }
								 return true;
								});
				    		
				    		
	}



}
