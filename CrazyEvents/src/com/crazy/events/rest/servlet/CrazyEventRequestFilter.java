package com.crazy.events.rest.servlet;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.crazy.events.queue.Producer;
import com.crazy.events.util.RateLimiter;
import com.crazy.events.util.ThreadPool;

@Provider
public class CrazyEventRequestFilter implements ContainerRequestFilter {
	
	static Logger log = Logger.getLogger(CrazyEventRequestFilter.class);

	@Override
	public void filter(ContainerRequestContext ctx) throws IOException {
						System.out.println("Inside filter");
						ScheduledThreadPoolExecutor scheduledThreadPool;
				
						boolean status = RateLimiter.validate();
				
						if (!status) {
							ctx.abortWith(Response.status(Response.Status.FORBIDDEN).entity("Cannot access").build());
						}

	}
}
