package com.crazy.events.rest.servlet;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.crazy.events.model.Event;
import com.crazy.events.model.Response;
import com.crazy.events.service.ICrazyEventService;
import com.crazy.events.service.impl.CrazyEventServiceImpl;

/**
 * REST servlet for the batch processing of events.
 * 
 *
 */
@Path("/events")
public class CrazyEventsRestServlet {
	
		static Logger log = Logger.getLogger(CrazyEventsRestServlet.class);
		
		/**
		 * GET method to get the status of the batch which is already uploaded using the POST method addEvents.
		 * The URI for this request can be obtained as part of the addEvents POST method response. 
		 * @param id batchId received in the response from the addEvents
		 * @return Response
		 */
		@GET
		@Produces (MediaType.APPLICATION_JSON)
		@Path("/{batchId}/getBatchStatus")
		public Response getBatchStatus(@PathParam("batchId") String id) {		
									ICrazyEventService eventService = new CrazyEventServiceImpl();	
									Response res = eventService.getBatchStatus(id);
									
									if (res.getBatchSize()==0){
										res.setMessage("Processing of batch with id "+ id +" is not complete yet");
									} else {
										res.setMessage("Processing of batch with "+ res.getBatchSize() +" events is complete");
									}
									res.setResponseCode("200");
									return res;
		 }  
		
		/**
		 * POST method for processing List of event.
		 * The API returns an initial Response with the rest URI to check the status of the batch uploaded
		 * Process the batch asynchronously
		 * @param eventList List of Event objects as JSON
		 * @return Response
		 */
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Path("/addEvents")
		public Response addEvents(List<Event> eventList) {
			
										Response res = new Response(); 
										
										ICrazyEventService eventService = new CrazyEventServiceImpl();
										
										if(eventList == null || eventList.size()==0){
									        res.setStatus(false);
									        res.setResponseCode("400");
									        res.setMessage("Please ensure the event list in the input json is not null");
									    }
									    try {
									    	res = eventService.addEvents(eventList);
									    	if (res!=null) {
									    		res.setStatus(true);
										        res.setResponseCode("200");
										        res.setMessage("Your event processing is in-progress, please check the status at :"
													+ new URI("/rest/"+ res.getBatchid()) +"/getBatchStatus");
									    	}
										} catch (URISyntaxException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
											res.setStatus(false);
									        res.setResponseCode("400");
									        res.setMessage("There was an error processing your request");
										}
									    return res;
									    
		}
			
}
