package com.crazy.events.command;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.crazy.events.enums.EventType;
import com.crazy.events.model.Event;
import com.crazy.events.queue.Consumer;
import com.crazy.events.queue.EventProducerConsumer;
import com.crazy.events.queue.EventThreeBlockingQueue;
import com.crazy.events.queue.IEventsBlockingQueue;
import com.crazy.events.queue.Producer;
import com.crazy.events.util.ThreadPool;

public class EventTypeThreeCommand implements ICommand{

		static Logger log = Logger.getLogger(EventTypeThreeCommand.class);
	 
		public static ThreadPoolExecutor threadPoolExecutor = ThreadPool.getInstance().getThreadPoolExecutor();
		IEventsBlockingQueue<Event> queue = EventThreeBlockingQueue.getInstance();

		

		@Override
		public void execute(List<Event> events) {
						EventType eventType = EventType.EVENT_THREE;
						threadPoolExecutor.execute(new Producer(queue, events));
						threadPoolExecutor.execute(new Consumer(queue, eventType.getSinkType().get(eventType)));
			//			ThreadPool.getInstance().getScheduledThreadPool().scheduleAtFixedRate(
			//					new EventProducerConsumer(queue, eventType.getSinkType().get(eventType)), 0, 15,
			//					TimeUnit.SECONDS);
						Timer timer = new Timer(); 
				        TimerTask task = new EventProducerConsumer(queue, eventType.getSinkType().get(eventType));  
				        timer.scheduleAtFixedRate(task, 0, 15000L); 
					}
		

}
