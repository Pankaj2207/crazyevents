package com.crazy.events.command;

import java.util.List;
import java.util.Map;

import com.crazy.events.enums.EventType;
import com.crazy.events.model.Event;

public interface ICommandRouter {
	
	public void route(Map<EventType, List<Event>> map) throws Exception;

}
