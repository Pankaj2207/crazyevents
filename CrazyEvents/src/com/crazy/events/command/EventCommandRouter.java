package com.crazy.events.command;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.crazy.events.enums.EventType;
import com.crazy.events.model.Event;

public class EventCommandRouter implements ICommandRouter {
	
	static Logger log = Logger.getLogger(EventCommandRouter.class);

	private static final String PACKAGE = "com.crazy.events.command.";

	private static final String COMMAND = "Command";

	public void route(Map<EventType, List<Event>> map){
							Set<EventType> keys = map.keySet();
							Iterator<EventType> iter = keys.iterator();
							while (iter.hasNext()) {
								try {
									EventType e = iter.next();
									Class<?> cls = Class.forName(PACKAGE + e.getEventType().get(e) + COMMAND);
									ICommand commandObj = (ICommand) cls.getConstructor().newInstance();
									cls.getMethod("execute",List.class).invoke(commandObj, map.get(e));
					
								} catch (Throwable t) {
									log.error("Error occurred in command routing for the event type ", t);
								}
							}

	}
}
