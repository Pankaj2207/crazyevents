package com.crazy.events.command;

import java.util.List;

import com.crazy.events.model.Event;

public interface ICommand {

	public void execute(List<Event> events);
}
