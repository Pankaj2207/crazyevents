package com.crazy.events.queue;

import org.apache.log4j.Logger;

import com.crazy.events.async.task.impl.EventAsyncTskImpl;
import com.crazy.events.enums.SinkType;
import com.crazy.events.model.Event;

public class Consumer implements Runnable{
	
	static Logger log = Logger.getLogger(Consumer.class);
	
	IEventsBlockingQueue<Event> queue;
	SinkType sinktype;

	public Consumer(IEventsBlockingQueue<Event> queue, SinkType sinktype) {
					this.queue = queue;
					this.sinktype = sinktype;
	}

	@Override
	public void run() {
					EventProducerConsumer pc = new EventProducerConsumer(queue,sinktype);
					try {
						pc.consume();
					} catch (InterruptedException e) {
						log.error("Error occurred while running the consumer thread", e);
					}
		
	}

}
