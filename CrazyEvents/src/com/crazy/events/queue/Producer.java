package com.crazy.events.queue;

import java.util.List;

import org.apache.log4j.Logger;

import com.crazy.events.enums.EventType;
import com.crazy.events.enums.SinkType;
import com.crazy.events.model.Event;

public class Producer implements Runnable{
	
	static Logger log = Logger.getLogger(Producer.class);
	
	
	private List<Event> events;
	
	IEventsBlockingQueue<Event> queue;
	SinkType sinkType;

	public Producer(IEventsBlockingQueue<Event> queue, List<Event> events) {
					EventType eventType = events.get(0).getEventType();
					this.events = events;
					this.queue = queue;
					sinkType = events.get(0).getEventType().getSinkType().get(eventType);
	}

	@Override
	public void run() {
					EventProducerConsumer pc = new EventProducerConsumer(queue, sinkType);
					for (Event event : events) {
						try {
							pc.produce(event);
						} catch (InterruptedException e) {
							log.error("Error occurred while running the producer thread", e);
						}
					}
	}
	

}
