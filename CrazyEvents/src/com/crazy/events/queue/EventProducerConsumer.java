package com.crazy.events.queue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TimerTask;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.log4j.Logger;

import com.crazy.events.enums.SinkType;
import com.crazy.events.model.Event;
import com.crazy.events.sink.SinkFactory;
import com.crazy.events.util.ThreadPool;

public class EventProducerConsumer extends TimerTask {
	
	static Logger log = Logger.getLogger(EventProducerConsumer.class);

	private IEventsBlockingQueue<Event> queue;

	public static ThreadPoolExecutor threadPoolExecutor = ThreadPool.getInstance().getThreadPoolExecutor();

	private Integer thresholdCapacity = 1000;

	private SinkType sinkType;

	public EventProducerConsumer(IEventsBlockingQueue<Event> queue, SinkType sinkType) {
		this.queue = queue;
		this.sinkType = sinkType;
	}

	@Override
	public void run() {
					Collection<Event> events = new ArrayList<Event>();
					if(queue.getQueue().size()!=0) {
						log.info("The scheduled sink job being executed at 15 sec");
						queue.getQueue().drainTo(events);
						SinkFactory.getSink(sinkType).sink(events);
					}
	}

	public void produce(Event event) throws InterruptedException {
					synchronized (this) {
						while (queue.getmaxCapacity() == thresholdCapacity) { // To be discussed
							wait();
						}
						log.info("Prodcer thread publishing on the queue");
						queue.getQueue().add(event);
						notifyAll();
					}
	}

	public Collection<Event> consume() throws InterruptedException {
						Collection<Event> events = new ArrayList<Event>();
						synchronized (this) {
							while (queue.getQueue().size() <= thresholdCapacity || queue.getQueue().isEmpty()) {
								wait();
							}
							log.info(" Consumer thread fetching from the queue, the size of the queue is " 
							+ queue.getQueue().size());
							queue.getQueue().drainTo(events);
							SinkFactory.getSink(sinkType).sink(events);
							notifyAll();
						}
						return events;
	}

	public SinkType getSinkType() {
						return sinkType;
	}

	public void setSinkType(SinkType sinkType) {
						this.sinkType = sinkType;
	}
}
