package com.crazy.events.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.crazy.events.model.Event;

public class EventTwoBlockingQueue<T> implements IEventsBlockingQueue<T>{
	
	static Logger log = Logger.getLogger(EventTwoBlockingQueue.class);
	
	private static BlockingQueue<Event> queue = null;

	private static Integer capacity = 1000;

	private static EventTwoBlockingQueue<Event> sInstance = null;

	private EventTwoBlockingQueue() {

	}

	public static IEventsBlockingQueue<Event> getInstance() {
						if (sInstance == null) {
							synchronized (EventTwoBlockingQueue.class) {
								sInstance = new EventTwoBlockingQueue<Event>();
								queue = new LinkedBlockingQueue<Event>();
							}
						}
						return sInstance;
	}


	@Override
	public BlockingQueue<Event> getQueue() {
		return queue;
	}

	@Override
	public Integer getmaxCapacity() {
		return capacity;
	}


}
