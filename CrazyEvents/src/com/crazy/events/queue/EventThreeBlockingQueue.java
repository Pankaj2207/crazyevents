package com.crazy.events.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.crazy.events.model.Event;

public class EventThreeBlockingQueue<T> implements IEventsBlockingQueue<T>{

	static Logger log = Logger.getLogger(EventThreeBlockingQueue.class);
	
	private static BlockingQueue<Event> queue = null;

	private static Integer capacity = 1000;

	private static EventThreeBlockingQueue<Event> sInstance = null;

	private EventThreeBlockingQueue() {

	}

	public static IEventsBlockingQueue<Event> getInstance() {
					if (sInstance == null) {
						synchronized (EventThreeBlockingQueue.class) {
							sInstance = new EventThreeBlockingQueue<Event>();
							queue = new LinkedBlockingQueue<Event>();
						}
					}
					return sInstance;
	}


	@Override
	public BlockingQueue<Event> getQueue() {
		return queue;
	}

	@Override
	public Integer getmaxCapacity() {
		return capacity;
	}

}
