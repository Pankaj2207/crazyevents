package com.crazy.events.queue;

import java.util.concurrent.BlockingQueue;

import com.crazy.events.model.Event;

public interface IEventsBlockingQueue<T> {
	
	public BlockingQueue<Event> getQueue();
	
	public Integer getmaxCapacity();

}
