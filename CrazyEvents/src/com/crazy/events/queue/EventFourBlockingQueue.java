package com.crazy.events.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.crazy.events.model.Event;

public class EventFourBlockingQueue<T> implements IEventsBlockingQueue<T>{
	
	static Logger log = Logger.getLogger(EventFourBlockingQueue.class);

	private static BlockingQueue<Event> queue = null;

	private static Integer capacity = 1000;

	private static EventFourBlockingQueue<Event> sInstance = null;

	private EventFourBlockingQueue() {

	}

	public static IEventsBlockingQueue<Event> getInstance() {
					if (sInstance == null) {
						synchronized (EventFourBlockingQueue.class) {
							sInstance = new EventFourBlockingQueue<Event>();
							queue = new LinkedBlockingQueue<Event>();
						}
					}
					return sInstance;
	}


	@Override
	public BlockingQueue<Event> getQueue() {
		return queue;
	}

	@Override
	public Integer getmaxCapacity() {
		return capacity;
	}


}
