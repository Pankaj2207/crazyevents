package com.crazy.events.util;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class ThreadPool {
	
	static Logger log = Logger.getLogger(ThreadPool.class);
	
	public static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
	
	private static ThreadPool sInstance;
	
	private static ThreadPoolExecutor threadPoolExecutor = null;
	
	private static ScheduledThreadPoolExecutor scheduledThreadPool = null;
	
	public static ScheduledThreadPoolExecutor getScheduledThreadPool() {
		return scheduledThreadPool;
	}

	public static void setScheduledThreadPool(ScheduledThreadPoolExecutor scheduledThreadPool) {
		ThreadPool.scheduledThreadPool = scheduledThreadPool;
	}

	public ThreadPoolExecutor getThreadPoolExecutor() {
		return threadPoolExecutor;
	}

	public static void setThreadPoolExecutor(ThreadPoolExecutor threadPoolExecutor) {
		ThreadPool.threadPoolExecutor = threadPoolExecutor;
	}

	public static ThreadPool getInstance() {
			        if (sInstance == null) {
			            synchronized (ThreadPool.class) {
			            	
			            	sInstance = new ThreadPool();
							threadPoolExecutor = new ThreadPoolExecutor(NUMBER_OF_CORES + 1, NUMBER_OF_CORES * 2, 60L,
									TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), Executors.defaultThreadFactory());
							
							scheduledThreadPool = new ScheduledThreadPoolExecutor(2);
			
							scheduledThreadPool.scheduleAtFixedRate(new RateLimiter(),
									0, 1, TimeUnit.MINUTES);
							
			            }
			        }
			        return sInstance;
    }
	
	private ThreadPool(){
		
	}
	
}
