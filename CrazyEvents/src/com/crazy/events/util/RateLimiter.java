package com.crazy.events.util;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.crazy.events.sink.SinkFactory;

public class RateLimiter implements Runnable{
	
	static Logger log = Logger.getLogger(RateLimiter.class);
	
    private static final AtomicInteger minuteRequestCount = new AtomicInteger();
	
	
	private static final BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(60);

	public RateLimiter() {
	}

	@Override
	public void run() {
				queue.add(minuteRequestCount.get());
		    	minuteRequestCount.set(0);
		
	}
	
	public static boolean validate () {
		    	if (minuteRequestCount.get() < 1000) {
		    		minuteRequestCount.incrementAndGet();
		    	} else {
		    		
		    		return false;
		    	}
		    	Integer sum = queue.stream()
		    			  .mapToInt(Integer::intValue)
		    			  .sum();
		    	
		    	if (sum >50000) {
		    		return false;
		    	}
		    	return true;
	}

}
