package com.crazy.events.util;

import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class TimerUtil extends TimerTask{

	AtomicInteger minuteRequestCount; 
	
	public TimerUtil(AtomicInteger minuteRequestCount){
		this.minuteRequestCount = minuteRequestCount;
	}
	
	@Override
	public void run() {
		minuteRequestCount.set(0);
		
	}

}
