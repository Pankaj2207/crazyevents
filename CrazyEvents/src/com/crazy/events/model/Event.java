package com.crazy.events.model;

import com.crazy.events.enums.EventType;

public class Event {

	private Long Id;

	private EventType eventType;

	private EventDetail eventDetail;

	private String batchId;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public EventDetail getEventDetail() {
		return eventDetail;
	}

	public void setEventDetail(EventDetail eventDetail) {
		this.eventDetail = eventDetail;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	@Override
	public String toString() {
		return Id + " " + eventType.getStrValue() + " " + batchId + " " + eventDetail.toString();

	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || obj.getClass() != this.getClass())
			return false;

		Event event = (Event) obj;
		return (event.eventType == this.eventType && event.Id == this.Id);
	}

}
