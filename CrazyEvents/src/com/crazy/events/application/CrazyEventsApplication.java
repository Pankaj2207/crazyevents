package com.crazy.events.application;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/*")
public class CrazyEventsApplication extends ResourceConfig{
	
	public CrazyEventsApplication() {
		packages("com.crazy.events.rest.filter");
	}

}
