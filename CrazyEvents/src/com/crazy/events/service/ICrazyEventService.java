package com.crazy.events.service;

import java.util.List;

import com.crazy.events.model.Event;
import com.crazy.events.model.Response;

public interface ICrazyEventService {

	Response getBatchStatus(String id);

	Response addEvents(List<Event> eventList);

}
