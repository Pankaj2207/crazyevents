package com.crazy.events.service.impl;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.crazy.events.async.task.impl.EventAsyncTskImpl;
import com.crazy.events.data.access.object.IJDBCDataAccessObject;
import com.crazy.events.data.access.object.PostgresDaoImpl;
import com.crazy.events.enums.EventType;
import com.crazy.events.model.Event;
import com.crazy.events.model.Response;
import com.crazy.events.rest.servlet.CrazyEventsRestServlet;
import com.crazy.events.service.ICrazyEventService;
import com.crazy.events.util.ThreadPool;


public class CrazyEventServiceImpl implements ICrazyEventService{
	
	static Logger log = Logger.getLogger(CrazyEventServiceImpl.class);
	
	ThreadPoolExecutor threadPoolExecutor = ThreadPool.getInstance().getThreadPoolExecutor();// member or local?
	
	@Override
	public Response getBatchStatus(String id) {
							Response response = new Response();
							IJDBCDataAccessObject dao = new PostgresDaoImpl();
							Long count = dao.getStatus(id);
							if (count == 0) {
								log.info("The batch status is not uploaded yet for the batch id "+id);
							}
							response.setBatchSize(count);
							response.setBatchid(id);
							return response;
							}
		

	@Override
	public Response addEvents(List<Event> eventList) {
		
							String batchId = Double.toString(Math.random());
							List<Event> events = eventList;
							events.stream().forEach(u -> u.setBatchId(batchId));
							
							Map<EventType, List<Event>> map = eventList
							        .stream()
							        .collect
							            (Collectors.groupingBy(Event::getEventType));
							
							threadPoolExecutor.execute(new EventAsyncTskImpl(map));
							
							Response response = new Response();
							response.setBatchid(batchId);
							return response;
		
		
	}
	
	
	

}
