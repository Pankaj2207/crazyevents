package com.crazy.events.async.task;

import org.apache.log4j.Logger;

public class TaskFactory {
	
	static Logger log = Logger.getLogger(TaskFactory.class);
	
	public static IAsyncTask getTask (String e, Class<?> param) {
						IAsyncTask obj = null;
						try {
							Class<?> cls = Class.forName("com.wholistic.web.task.impl."+e);
							obj = (IAsyncTask) cls.getConstructor(param).newInstance(1L);
							//Method method = (Method) cls.getMethod("execute").invoke(obj);
						} catch (Throwable t) {
							log.error("Error occurred while getting the async task from the task factory " + e, t);
						}
						
						return obj;
	}

}
