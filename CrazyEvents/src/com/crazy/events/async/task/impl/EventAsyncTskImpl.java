package com.crazy.events.async.task.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.crazy.events.async.task.IAsyncTask;
import com.crazy.events.command.EventCommandRouter;
import com.crazy.events.command.ICommandRouter;
import com.crazy.events.enums.EventType;
import com.crazy.events.model.Event;

public class EventAsyncTskImpl implements IAsyncTask {
	
	private Map<EventType,List<Event>> map;

	static Logger log = Logger.getLogger(EventAsyncTskImpl.class);
	
	public EventAsyncTskImpl(Map<EventType,List<Event>> map ) {
		this.map = map;
	}
	
	@Override
	public void run() {
				ICommandRouter router = new EventCommandRouter();
				try {
					router.route(getMap());
				} catch (Exception e) {
					log.error("Error forking the async task while the command is being routed ", e);
					e.printStackTrace();
				}
	}
	
	public Map<EventType, List<Event>> getMap() {
		return map;
	}

	public void setMap(Map<EventType, List<Event>> map) {
		this.map = map;
	}

}
