package com.crazy.events.enums;

import java.util.HashMap;
import java.util.Map;

public enum EventType {

	EVENT_ONE("EventTypeOne", SinkType.LOGFILE_SINK), 
	EVENT_TWO("EventTypeTwo", SinkType.LOGFILE_SINK), 
	EVENT_THREE("EventTypeThree", SinkType.LOGFILE_SINK), 
	EVENT_FOUR("EventTypeFour", SinkType.LOGFILE_SINK);

	private String strValue;

	private SinkType sinkType;

	private EventType(String name, SinkType sinkType) {
		this.strValue = name;
		this.sinkType = sinkType;
	}

	private static Map<EventType, String> map = new HashMap<EventType, String>();

	private static Map<EventType, SinkType> sinkMap = new HashMap<EventType, SinkType>();

	public static Map<EventType, String> getEventType() {
		EventType[] arr = EventType.values();

		for (EventType eventType : arr) {
			map.put(eventType, eventType.strValue);
		}
		return map;
	}

	public static Map<EventType, SinkType> getSinkType() {
		EventType[] arr = EventType.values();

		for (EventType eventType : arr) {
			sinkMap.put(eventType, eventType.sinkType);
		}
		return sinkMap;
	}

	public String getStrValue() {
		return strValue;
	}

	public void setStrValue(String strValue) {
		this.strValue = strValue;
	}

}
