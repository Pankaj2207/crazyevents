package com.crazy.events.enums;

import java.util.HashMap;
import java.util.Map;

public enum SinkType {
	
	KAFKA_SINK("KafkaTopicSink"),
	LOGFILE_SINK("LogFileSink");
	
	private String name;
	
	private Map<SinkType, String> map = new HashMap<SinkType, String>();

	
	private SinkType (String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Map<SinkType, String> getSinkType() {
		SinkType[] arr = SinkType.values();

		for (SinkType sinkType : arr) {
			map.put(sinkType, sinkType.getName());
		}
		return map;
	}

}
