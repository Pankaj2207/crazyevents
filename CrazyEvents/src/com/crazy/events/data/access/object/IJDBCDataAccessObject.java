package com.crazy.events.data.access.object;

import java.util.List;

import com.crazy.events.model.Event;

public interface IJDBCDataAccessObject extends IDataAccessObject{
		
	public Long getStatus(String id) ;
	
	public void insertEvents(List<Event> list) ;

}
