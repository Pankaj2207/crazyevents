package com.crazy.events.data.access.object;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.crazy.events.command.EventTypeTwoCommand;
import com.crazy.events.model.Event;
import com.crazy.events.util.DBCPDataSource;

public class PostgresDaoImpl implements IJDBCDataAccessObject {

	static Logger log = Logger.getLogger(PostgresDaoImpl.class);

	public Long getStatus(String id) {
						Long count = 0L;
						try {
							Connection connection = DBCPDataSource.getConnection();
							StringBuilder str = new StringBuilder();
							str.append("SELECT count(*) FROM crazy_events.crazy_events_detail where batch_id=?;");
				
							PreparedStatement stmt = connection.prepareStatement(str.toString());
							stmt.setString(1, id);
				
							ResultSet rs = stmt.executeQuery();
							
							if (rs.next()) {
								count = rs.getLong(1);
							} 
							return count;
				
				
						} catch (Throwable e) {
							log.error("Error occured while fetching the status data for the batch id "+ id, e);
							return count;
						}

	}

	public void insertEvents(List<Event> list) {
						try {
							Connection connection = DBCPDataSource.getConnection();
				
							for (Event event : list) {
								StringBuilder str = new StringBuilder();
								str.append(
										"INSERT INTO crazy_events.crazy_events_detail(id, event_type, event_message, batch_id, event_name)VALUES (?, ?, ?, ?, ?)");
				
								PreparedStatement stmt = connection.prepareStatement(str.toString());
								stmt.setInt(1, event.getId().intValue());
								stmt.setString(2, event.getEventType().getStrValue());
								stmt.setString(3, event.getEventDetail().getValue());
								stmt.setString(4, event.getBatchId());
								stmt.setString(5, event.getEventDetail().getName());
				
								int rs = stmt.executeUpdate();
								log.info("The status data update is successfull with the return as "+rs);
				
							}
						} catch (Throwable e) {
							log.error("Error occured during the status data update ", e);
						}
					}

}
